#include <iostream>

using namespace std;


class Classe{
	double bi;
	double bs;
	unsigned q;
	public:
		
		Classe(const double& a, const double& b){
			this->bi = a;
			this->bs = b;
			this->q = 0;
		}
		double getBorneInf() const{
			return this->bi;
		}
		double getBorneSup() const{
			return this->bs;
		}
		unsigned getQuantite() const{
			return this->q;
		}
		void setQuantite(const unsigned& quantite){
			this->q = quantite;
		}
		void setBorneInf(const double& a){
			this->bi=a;
		}
		void setBorneSup(const double& a){
			this->bs=a;
		}
		void ajouter(){
			this->q += 1;
		}
		inline bool operator> (const Classe& a) const { return (this->bi)>(a.getBorneInf()); }
		

};




