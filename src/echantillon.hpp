#pragma once

#include <algorithm>
#include <vector>
#include <iterator>
#include "valeur.hpp"

class Echantillon{
	private:
		vector<Valeur> vecteur;
	public:
		
		unsigned getTaille(){
			return vecteur.size();
		}
		void ajouter(Valeur v){
			vecteur.push_back(v);
		}
		static bool compare( Valeur a,  Valeur b)
		{
		    return (a.getNombre() < b.getNombre());
		}
		Valeur& getMaximum(){
			if(vecteur.size() > 0){
			vector<Valeur>::iterator it = max_element(this->vecteur.begin(),this->vecteur.end(),compare);
				return *it;
			} else {
				throw std::domain_error("Erreur");
			}
			
		}
		Valeur& getMinimum(){
		        if(vecteur.size() > 0){
			vector<Valeur>::iterator it = min_element(this->vecteur.begin(),this->vecteur.end(),compare);
			return *it;
		}else{
			throw std::domain_error("Erreur");
		}
		
		}

		Valeur& getValeur(unsigned indice){
			if(indice >= vecteur.size()){
				throw std::out_of_range("out of range erreur");
			}else {
				return vecteur[indice];
			}
		}



};
