#pragma once 


#include "comparateur_quantite.hpp"
#include "echantillon.hpp"
#include "classe.hpp"
#include <algorithm>
#include <iterator>
#include <functional>   // std::greater
#include <vector>


template <typename... Ts> class Histogramme{
	
	vector<Classe> vecteur;
	public:
		typedef vector<Classe> classes_t;
		Histogramme(double bi, double bs, unsigned nbre){
			
			
			double pas = (bs-bi)/(double)nbre;
			for (unsigned i=0;i<nbre;i++){
				Classe c(bi, bi+pas);	
				vecteur.push_back(c);
				bi += pas;
			}
		}

		classes_t& getClasses(){
			return this->vecteur;
		}
		void ajouter(Echantillon& e){
			for (unsigned i=0; i<e.getTaille(); i++){
				Valeur v = e.getValeur(i);
				for(unsigned j=0; j<(this->vecteur).size(); j++){
					if(v.getNombre()>= vecteur[j].getBorneInf() && v.getNombre() < vecteur[j].getBorneSup()){
						vecteur[j].ajouter();
					}
				}
			}		
		}
};

template <> Histogramme< std::greater<Classe> >::Histogramme(double bi, double bs, unsigned nbre)
{
			
			cout << "je suis appellé" << endl;
			double pas = (bs-bi)/(double)nbre;
			for (unsigned i=0;i<nbre;i++){
				Classe c(bi, bi+pas);	
				vecteur.push_back(c);
				bi += pas;
			}
			std::sort(vecteur.begin(), vecteur.end(), std::greater<Classe>());
			cout << "premier membre" << vecteur[0].getQuantite() << endl;;
		
}


template <> Histogramme< ComparateurQuantite<Classe> >::Histogramme(double bi, double bs, unsigned nbre)
{
			
			cout << "je suis appellé" << endl;
			double pas = (bs-bi)/(double)nbre;
			for (unsigned i=0;i<nbre;i++){
				Classe c(bi, bi+pas);	
				vecteur.push_back(c);
				bi += pas;
			}
			std::sort(vecteur.begin(), vecteur.end(), std::greater<ComparateurQuantite<Classe> >());
		
}


