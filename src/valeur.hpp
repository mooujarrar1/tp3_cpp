#pragma once

#include <iostream>
#include <stdexcept>

using namespace std;


class Valeur{

	private:
		double val;
	public:
		Valeur(const double& a){
			this->val = a;
		}
		Valeur(){
			this->val = 0.0;
		}
		double getNombre() const{
			return this->val;
		}
		void setNombre(const double& a){
			this->val=a;
		}
};



